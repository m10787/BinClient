#pragma once
#include "Includes.h"

class CThreadPool
{
public:
	CThreadPool(uint32_t numThreads);
	
	// adds new task to Thread Pool
	template <typename Func, typename ...Args>
	int64_t addTask(const Func& taskFunc, Args... args)
	{
		// getting next task ID
		int64_t taskID = lastID++;

		std::lock_guard<std::mutex> q_lock(q_mtx);
		q.emplace(std::async(std::launch::deferred, taskFunc, args...), taskID);

		// waking up one thread in run method
		q_cv.notify_one();
		return taskID;
	}

	// waits for task execution
	void wait(int64_t taskID);
	// waits all tasks execution
	void waitAll();

	// checks if the task is executed
	bool executed(int64_t taskID);

	~CThreadPool();

private:
	// launch Thread Pool
	void run();

	// task queue, stores function(task) and its id
	std::queue<std::pair<std::future<void>, int64_t>> q;

	std::mutex q_mtx;
	std::condition_variable q_cv;

	// stores completed tasks
	std::unordered_set<int64_t> completedTaskIDs;

	std::condition_variable completedTaskIDsCv;
	std::mutex completedTaskIDsMtx;

	std::vector<std::thread> threads;

	// ThreadPool shutdown flag
	std::atomic<bool> quite{ false };

	// Next task's ID
	std::atomic<int64_t> lastID = 0;
};
