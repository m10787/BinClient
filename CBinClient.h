#pragma once
#include "Includes.h"

// Class Client
// Runs on a separate thread
// Sends all dumps from dumpVec
class CBinClient
{
public:
	CBinClient(std::string address, unsigned short port, std::vector<std::string> dumpVec);
	~CBinClient();	
	// Launch Client
	void run();
	// Stop Client
	int stop();
	// Hex-string/char-string converter
	std::string HexToBytes(const std::string& hex);

private:

	// Client handler
	void handle();

	// Address
	std::string m_endpAddr;
	// Port
	unsigned short m_endpPort;
	// Dump Vector, contains all dumps
	std::vector<std::string> m_dumpVec;
	// Dump
	std::string m_dump;
	// Client's socket descriptor
	int m_sockfd;
};