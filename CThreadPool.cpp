#include "CThreadPool.h"

CThreadPool::CThreadPool(uint32_t numThreads)
{
	threads.reserve(numThreads);
	for (uint32_t i = 0; i < numThreads; ++i)
	{
		threads.emplace_back(&CThreadPool::run, this);
	}
}

void CThreadPool::run()
{
	while (!quite)
	{
		std::unique_lock<std::mutex> lock(q_mtx);

		// if got new task - executes it
		// if ThreadPool is destructed - goes to the end of method missing the cycle 
		q_cv.wait(lock, [this]()->bool {return !q.empty() || quite; });

		if (!q.empty())
		{
			auto elem = std::move(q.front());
			q.pop();
			lock.unlock();

			// executing task
			elem.first.get();

			std::lock_guard<std::mutex> lock(completedTaskIDsMtx);

			// the task is completed
			completedTaskIDs.insert(elem.second);

			// waking up the threads
			completedTaskIDsCv.notify_all();
		}
	}
}

void CThreadPool::wait(int64_t taskID)
{
	std::unique_lock<std::mutex> lock(q_mtx);

	// waiting for notify in run method
	completedTaskIDsCv.wait(lock, [this, taskID]()->bool {
		return completedTaskIDs.find(taskID) != completedTaskIDs.end();
		});
}

void CThreadPool::waitAll()
{
	std::unique_lock<std::mutex> lock(q_mtx);

	// waiting for notify in run method
	completedTaskIDsCv.wait(lock, [this]()->bool {
		std::lock_guard<std::mutex> lock(completedTaskIDsMtx);
		return q.empty() && lastID == completedTaskIDs.size();
		});
}

bool CThreadPool::executed(int64_t taskID)
{
	std::lock_guard<std::mutex> lock(completedTaskIDsMtx);

	if (completedTaskIDs.find(taskID) != completedTaskIDs.end())
		return true;
	return false;
}

CThreadPool::~CThreadPool()
{
	// waiting for all threads(optionally, can be coommented)
	waitAll();

	quite = true;
	for (uint32_t i = 0; i < threads.size(); ++i)
	{
		q_cv.notify_all();
		threads[i].join();
	}
}