#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "CBinClient.h"

CBinClient::CBinClient(std::string address, unsigned short port, std::vector<std::string> dumpVec) :
	m_endpAddr(address), m_endpPort(port)
{
	// Initializing dump vector
	m_dumpVec.reserve(dumpVec.size());
	std::copy(dumpVec.begin(), dumpVec.end(), std::back_inserter(m_dumpVec));
}

CBinClient::~CBinClient()
{

}

void CBinClient::run()
{
	// Service structure for storing the implementation of WindowsSocket
	WSADATA wsadata;

	int result = WSAStartup(MAKEWORD(2, 2), &wsadata);

	// If an error occured
	if (result != 0)
	{
		std::cout << "WSAStartup failed: " << result << std::endl;
		return;
	}

	// Creating socket
	m_sockfd = socket(AF_INET, SOCK_STREAM, 0);

	// If an error occured
	if (m_sockfd == INVALID_SOCKET)
	{
		std::cout << "INVALID SOCKET: " << WSAGetLastError() << std::endl;
		WSACleanup();
		return;
	}

	// Socket address structure
	struct sockaddr_in servaddr;

	// Filling socket address structure
	ZeroMemory(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(m_endpPort);
	inet_pton(AF_INET, m_endpAddr.c_str(), &servaddr.sin_addr);

	// Connecting to server
	result = connect(m_sockfd, (sockaddr*)&servaddr, sizeof(servaddr));
	
	if (result != 0)
	{
		std::cout << "Connection Error\n";
		closesocket(m_sockfd);
		return;
	}

	handle();
}

void CBinClient::handle()
{
	// Iterator, points the next dump
	std::vector<std::string>::iterator iter = m_dumpVec.begin();

	// Sending all dumps
	while (iter != m_dumpVec.end())
	{
		m_dump = *iter;

		// Converting dump to hex representation
		m_dump = HexToBytes(m_dump);

		// Sending message
		send(m_sockfd, m_dump.c_str(), strlen(m_dump.c_str()), 0);

		// Getting an answer
		// Received bytes
		int nread;
		// Answer buffer(8 KB)
		char bufAnswer[8192];

		nread = recv(m_sockfd, bufAnswer, sizeof(bufAnswer), 0);

		// The answer handler Here(if needed)

		++iter;
	}
	
	stop();
}

int CBinClient::stop()
{
	closesocket(m_sockfd);
	WSACleanup();
	return 0;
}

std::string CBinClient::HexToBytes(const std::string& hex) 
{
	std::string bytes;

	for (unsigned int i = 0; i < hex.length(); i += 2) {
		std::string byteString = hex.substr(i, 2);
		char byte = (char)strtol(byteString.c_str(), NULL, 16);
		bytes.push_back(byte);
	}

	return bytes;
}