Пример входных данных для запуска программы из командной строки:
В однопоточном режиме:
BinClient.exe 127.0.0.1 3456 5 1 C:\Users\denis.osipov\source\repos\NetworkTraining\BinClient\BinClient\dumps
В многопоточном режиме:
BinClient.exe 127.0.0.1 3456 20 5 C:\Users\denis.osipov\source\repos\NetworkTraining\BinClient\BinClient\dumps