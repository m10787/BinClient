#define _CRT_SECURE_NO_WARNINGS

#include "CBinClient.h"
#include "CThreadPool.h"

// For correct work we need to link Ws2_32.lib
#pragma comment (lib, "ws2_32.lib")

using namespace std;
namespace fs = std::filesystem;

// Repository passing-by function
// vec - vector stores dumps from files 
// dumpAddress - address of repo with dumps
int GetAllDumps(vector<string>& vec, string dumpAddress)
{
	// If repo doesn't exist
	if (!fs::exists(dumpAddress.c_str()))
		return -1;
	
	fs::path p = dumpAddress.c_str();

	// Repo passing-by
	for (fs::directory_entry entry : fs::directory_iterator(p)) 
	{
		ifstream in(entry.path().string().c_str(), ios::binary);
		if (!in)
		{
			cout << entry.path().string().c_str() << " can't be opened\n";
			continue;
		}

		// Buffer Size
		const int BUFSIZE = 2048;
		char buf[BUFSIZE];
		int nread = 0;

		// Reading file
		in.read(buf, BUFSIZE);
		while (in.gcount() > 0)
		{		
			nread += in.gcount();
			in.read(buf, BUFSIZE);
		}
		buf[nread] = '\0';
		vec.push_back(static_cast<string>(buf));
		in.close();
	}

	return 0;
}

// Arguments:
// endpoint address
// endpoint port
// request count(clients count)
// thread count
// path to the repository containing the payload
int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");

	if (argc != 6)
	{
		cout << "Wrong number of arguments\n";
		cout << "<IP Address> <Port> <Request Count> <Thread Count> <DumpFolder Address>\n";
		return -1;
	}
	
	// Endpoint Address
	string endpAddress = argv[1];
	// Endpoint Port
	unsigned short endpPort = atoi(argv[2]);
	if (endpPort == 0)
		cout << "Incorrect Port Value\n";
	// Request Count
	int maxRequestCount = atoi(argv[3]);
	if (maxRequestCount == 0)
		cout << "Incorrect Request Count Value\n";
	// Thread Count
	int maxThreadCount = atoi(argv[4]);
	if (maxThreadCount == 0)
		cout << "Incorrect Thread Count Value\n";
	// Dump Repo Address
	string dumpAddress = argv[5];

	// Dump vector
	vector<string> dumpVec;

	if (GetAllDumps(dumpVec, dumpAddress) == -1)
	{
		cout << "Dump repository doesn't exist\n";
		return -2;
	}

	// Creating vector that contains all clients
	vector<CBinClient*> clientVec;
	for (int i = 0; i < maxRequestCount; ++i)
	{
		CBinClient* clientPtr = new CBinClient(endpAddress, endpPort, dumpVec);
		clientVec.push_back(clientPtr);
	}

	// Creating Thread Pool
	CThreadPool tp(maxThreadCount);

	// Launching clients on different threads
	for (CBinClient* client : clientVec)
	{
		tp.addTask(&CBinClient::run, client);
	}

	tp.waitAll();

	for (CBinClient* client : clientVec)
	{
		delete client;
	}

	return 0;
}